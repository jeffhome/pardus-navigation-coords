// ==UserScript==
// @name        Pardus Navigation Coords
// @namespace   http://userscripts.xcom-alliance.info/
// @description Adds the coordinates for each tile to the navigation screen
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://*.pardus.at/main.php*
// @include     http*://*.pardus.at/building.php
// @version     1.5
// @updateURL 	http://userscripts.xcom-alliance.info/navigation_coords/pardus_navigation_coords.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/navigation_coords/pardus_navigation_coords.user.js
// @icon 		http://userscripts.xcom-alliance.info/navigation_coords/icon.png
// @grant       none
// ==/UserScript==