// ==UserScript==
// @name        Pardus Navigation Coords
// @namespace   http://userscripts.xcom-alliance.info/
// @description Adds the coordinates for each tile to the navigation screen
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://*.pardus.at/main.php*
// @include     http*://*.pardus.at/building.php
// @version     1.5
// @updateURL 	http://userscripts.xcom-alliance.info/navigation_coords/pardus_navigation_coords.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/navigation_coords/pardus_navigation_coords.user.js
// @icon 		http://userscripts.xcom-alliance.info/navigation_coords/icon.png
// @grant       none
// ==/UserScript==

/*****************************************************************************************
	Version Information

	1.0 ( 16-Aug-2013 )
	- initial release for general use

	1.1 ( 16-Aug-2013 )
	- support for partial refresh

	1.2 ( 16-Aug-2013 )
	- compatibility with other scripts that modify the coords
	- display the tile coords with a space between the existing title (if one is already present)

	1.3 ( 16-Aug-2013 )
	- fixed a bug with partial refresh where all coords were offset by 1

	1.4 ( 27-Aug-2013 )
	- added nav coordinates to the building welcome screen

	1.5 ( 01-Dec-2014 )
	- support the new navigation screen sizes correctly

*****************************************************************************************/

var PardusNavCoords = {
	locx: 0,
	locy: 0,
	Init: function() {
		this.GetLocation();
		this.DecorateNavTiles(false);
	},
	Update: function() {
		this.GetLocation();
		this.DecorateNavTiles(true);
	},
	GetLocation: function() {
		var coords = document.getElementById('coords').textContent;
		coords = coords.substring(1, coords.length-1);
		this.locx = coords.split(',')[0];
		this.locy = coords.split(',')[1];
	},
	DecorateNavTiles: function(partial) {
		var localX = this.locx - ((navSizeHor - 1) / 2);
		var localY = this.locy - ((navSizeVer - 1) / 2);
		var xLimit = Math.round(this.locx) + (navSizeHor - 1) / 2;
		var img = document.querySelectorAll((partial ? '#navareatransition ' : '') + 'img.' + tileClass);
		for (var i=img.length>fieldsTotal?1:0; i<img.length; i++) {
			img[i].title += (img[i].title != '' ? ' ' : '') + '[' + localX + ',' + localY + ']';
			localX++;
			if (localX > xLimit) {
				localX = this.locx - ((navAreaSize - 1) / 2);
				localY++;
			}
		}
	}
};

if (document.URL.indexOf('pardus.at/main.php') > -1) {

	// run normally once
	PardusNavCoords.Init();

	// hook in to allow for partial refresh
	unsafeWindow.addUserFunction(function(PNC) {
		return function() {
			PNC.Update();
		};
	}(PardusNavCoords));

}

else

if (document.URL.indexOf('pardus.at/building.php') > -1) {

	function GetSectorCoords(loc) {
		var locref = {
			"228" : {"s":"Alpha Centauri", "c":19, "r":12},
			"528" : {"s":"Andexa", "c":20, "r":15},
			"889" : {"s":"Beta Proxima", "c":19, "r":19},
			"1089" : {"s":"BL 3961", "c":20, "r":10},
			"1173" : {"s":"Cesoho", "c":12, "r":7},
			"1293" : {"s":"Daceess", "c":15, "r":8},
			"1869" : {"s":"Epsilon Eridani", "c":18, "r":32},
			"2259" : {"s":"Ericon", "c":15, "r":26},
			"2627" : {"s":"Faexze", "c":23, "r":16},
			"2915" : {"s":"Hoanda", "c":16, "r":18},
			"2985" : {"s":"Lalande", "c":7, "r":10},
			"3353" : {"s":"Lave", "c":23, "r":16},
			"3493" : {"s":"Miarin", "c":7, "r":20},
			"3621" : {"s":"Olexti", "c":8, "r":16},
			"3846" : {"s":"Ook", "c":15, "r":15},
			"4116" : {"s":"Orerve", "c":18, "r":15},
			"4176" : {"s":"PJ 3373", "c":10, "r":6},
			"4496" : {"s":"Quurze", "c":16, "r":20},
			"4576" : {"s":"SA 2779", "c":16, "r":5},
			"4951" : {"s":"Siberion", "c":25, "r":15},
			"5647" : {"s":"Sol", "c":24, "r":29},
			"6022" : {"s":"Tau Ceti", "c":25, "r":15},
			"6141" : {"s":"WP 3155", "c":17, "r":7},
			"6333" : {"s":"XH 3819", "c":16, "r":12},
			"6408" : {"s":"ZZ 2986", "c":15, "r":5},
			"6980" : {"s":"Adaa", "c":22, "r":26},
			"7394" : {"s":"An Dzeve", "c":23, "r":18},
			"7538" : {"s":"IP 3-251", "c":16, "r":9},
			"7772" : {"s":"Canaab", "c":18, "r":13},
			"8272" : {"s":"Kenlada", "c":25, "r":20},
			"8356" : {"s":"Edeneth", "c":12, "r":7},
			"8556" : {"s":"Exinfa", "c":10, "r":20},
			"8764" : {"s":"Grecein", "c":13, "r":16},
			"9044" : {"s":"Iceo", "c":20, "r":14},
			"9164" : {"s":"Inliaa", "c":12, "r":10},
			"9714" : {"s":"Lamice", "c":25, "r":22},
			"10114" : {"s":"Lianla", "c":20, "r":20},
			"10450" : {"s":"Miayack", "c":24, "r":14},
			"10912" : {"s":"Olbea", "c":21, "r":22},
			"11237" : {"s":"Phicanho", "c":13, "r":25},
			"11517" : {"s":"PO 4-991", "c":20, "r":14},
			"11933" : {"s":"Quana", "c":16, "r":26},
			"12101" : {"s":"RV 2-578", "c":14, "r":12},
			"12601" : {"s":"Sigma Draconis", "c":25, "r":20},
			"13601" : {"s":"Ska", "c":40, "r":25},
			"13981" : {"s":"Tiexen", "c":19, "r":20},
			"14281" : {"s":"Urandack", "c":20, "r":15},
			"14409" : {"s":"VH 3-344", "c":8, "r":16},
			"14664" : {"s":"Waiophi", "c":17, "r":15},
			"14872" : {"s":"XC 3-261", "c":16, "r":13},
			"15052" : {"s":"Zeolen", "c":15, "r":12},
			"15552" : {"s":"Pass EMP-01", "c":20, "r":25},
			"15912" : {"s":"Pass EMP-02", "c":18, "r":20},
			"16218" : {"s":"Pass FED-01", "c":18, "r":17},
			"16465" : {"s":"Pass FED-02", "c":13, "r":19},
			"16825" : {"s":"Anaam", "c":18, "r":20},
			"17149" : {"s":"Ayargre", "c":18, "r":18},
			"17469" : {"s":"Beethti", "c":16, "r":20},
			"17707" : {"s":"Begreze", "c":17, "r":14},
			"18332" : {"s":"Canexin", "c":25, "r":25},
			"18617" : {"s":"Datiack", "c":19, "r":15},
			"18887" : {"s":"Edbeeth", "c":18, "r":15},
			"19143" : {"s":"Edmize", "c":16, "r":16},
			"19451" : {"s":"Faphida", "c":22, "r":14},
			"19871" : {"s":"Grefaho", "c":21, "r":20},
			"20443" : {"s":"Laolla", "c":12, "r":17},
			"21256" : {"s":"Olphize", "c":19, "r":21},
			"21391" : {"s":"QW 2-014", "c":15, "r":9},
			"21875" : {"s":"Retho", "c":22, "r":22},
			"22275" : {"s":"Soessze", "c":20, "r":20},
			"22407" : {"s":"TG 2-143", "c":11, "r":12},
			"22939" : {"s":"Tiacken", "c":19, "r":28},
			"23263" : {"s":"Urhoho", "c":18, "r":18},
			"23683" : {"s":"Usube", "c":14, "r":30},
			"24007" : {"s":"Zamith", "c":18, "r":18},
			"24457" : {"s":"Zirr", "c":25, "r":18},
			"24977" : {"s":"Ackandso", "c":26, "r":20},
			"25250" : {"s":"Aeg", "c":21, "r":13},
			"25458" : {"s":"Besoex", "c":13, "r":16},
			"25705" : {"s":"Cassand", "c":13, "r":19},
			"25915" : {"s":"Daurlia", "c":14, "r":15},
			"26293" : {"s":"Delta Pavonis", "c":14, "r":27},
			"26818" : {"s":"Eta Cassiopeia", "c":15, "r":35},
			"27013" : {"s":"Exackcan", "c":15, "r":13},
			"27413" : {"s":"Fomalhaut", "c":20, "r":20},
			"27735" : {"s":"Greandin", "c":14, "r":23},
			"27973" : {"s":"Iniolol", "c":17, "r":14},
			"28857" : {"s":"Keldon", "c":26, "r":34},
			"28953" : {"s":"KU 3-616", "c":12, "r":8},
			"29193" : {"s":"Laanex", "c":15, "r":16},
			"29229" : {"s":"LN 3-141", "c":6, "r":6},
			"29319" : {"s":"PI 4-669", "c":9, "r":10},
			"29519" : {"s":"Pollux", "c":20, "r":10},
			"29843" : {"s":"Quator", "c":18, "r":18},
			"30099" : {"s":"Regulus", "c":16, "r":16},
			"30183" : {"s":"SZ 4-419", "c":12, "r":7},
			"30468" : {"s":"Tianbe", "c":19, "r":15},
			"30740" : {"s":"Urlafa", "c":17, "r":16},
			"31070" : {"s":"Vewaa", "c":22, "r":15},
			"31187" : {"s":"WG 3-288", "c":9, "r":13},
			"31547" : {"s":"Wolf", "c":18, "r":20},
			"31687" : {"s":"Zezela", "c":14, "r":10},
			"32187" : {"s":"Pass EMP-03", "c":25, "r":20},
			"32487" : {"s":"Ackexa", "c":20, "r":15},
			"32727" : {"s":"Alioth", "c":16, "r":15},
			"33087" : {"s":"Bedaho", "c":20, "r":18},
			"33791" : {"s":"Betelgeuse", "c":32, "r":22},
			"34114" : {"s":"Capella", "c":19, "r":17},
			"34374" : {"s":"Epsilon Indi", "c":20, "r":13},
			"34616" : {"s":"Essaa", "c":11, "r":22},
			"34811" : {"s":"Famiay", "c":15, "r":13},
			"34955" : {"s":"GV 4-652", "c":12, "r":12},
			"35111" : {"s":"HC 4-962", "c":12, "r":13},
			"35405" : {"s":"Inena", "c":14, "r":21},
			"35535" : {"s":"JS 2-090", "c":13, "r":10},
			"35565" : {"s":"LO 2-014", "c":10, "r":3},
			"35825" : {"s":"Maia", "c":20, "r":13},
			"36225" : {"s":"Micanex", "c":20, "r":20},
			"36537" : {"s":"Nebul", "c":12, "r":26},
			"36837" : {"s":"Nionquat", "c":15, "r":20},
			"37141" : {"s":"Omicron Eridani", "c":16, "r":19},
			"37277" : {"s":"Phekda", "c":8, "r":17},
			"38002" : {"s":"Rashkan", "c":25, "r":29},
			"38226" : {"s":"Sohoa", "c":14, "r":16},
			"38496" : {"s":"Tiacan", "c":15, "r":18},
			"38896" : {"s":"Waayan", "c":25, "r":16},
			"39106" : {"s":"YC 3-268", "c":14, "r":15},
			"39274" : {"s":"Zeaex", "c":12, "r":14},
			"39529" : {"s":"Pass FED-03", "c":17, "r":15},
			"40079" : {"s":"Pass FED-04", "c":25, "r":22},
			"40520" : {"s":"Pass FED-05", "c":21, "r":21},
			"40934" : {"s":"Pass FED-06", "c":18, "r":23},
			"41142" : {"s":"Betiess", "c":13, "r":16},
			"41367" : {"s":"BQ 3-927", "c":15, "r":15},
			"41653" : {"s":"Canopus", "c":13, "r":22},
			"42303" : {"s":"Daaya", "c":26, "r":25},
			"42671" : {"s":"Electra", "c":23, "r":16},
			"42923" : {"s":"Enaness", "c":21, "r":12},
			"43253" : {"s":"Famiso", "c":22, "r":15},
			"43576" : {"s":"Hocancan", "c":17, "r":19},
			"43956" : {"s":"Laedgre", "c":19, "r":20},
			"44352" : {"s":"Miphimi", "c":22, "r":18},
			"44852" : {"s":"Nex 0002", "c":20, "r":25},
			"45252" : {"s":"Olcanze", "c":20, "r":20},
			"45525" : {"s":"Phiagre", "c":21, "r":13},
			"46253" : {"s":"Remo", "c":28, "r":26},
			"46508" : {"s":"Ross", "c":17, "r":15},
			"46945" : {"s":"SD 3-562", "c":23, "r":19},
			"47285" : {"s":"Ururur", "c":20, "r":17},
			"47472" : {"s":"WO 3-290", "c":17, "r":11},
			"48097" : {"s":"Nex Kataam", "c":25, "r":25},
			"48262" : {"s":"HO 2-296", "c":15, "r":11},
			"48509" : {"s":"Iozeio", "c":19, "r":13},
			"52082" : {"s":"Mizar", "c":16, "r":23},
			"52462" : {"s":"AN 2-956", "c":19, "r":20},
			"52700" : {"s":"Becanin", "c":17, "r":14},
			"52898" : {"s":"Cabard", "c":9, "r":22},
			"53168" : {"s":"Cemiess", "c":18, "r":15},
			"53378" : {"s":"Encea", "c":14, "r":15},
			"54003" : {"s":"Exbeur", "c":25, "r":25},
			"54371" : {"s":"Facece", "c":16, "r":23},
			"54566" : {"s":"GM 4-572", "c":15, "r":13},
			"55091" : {"s":"Lahola", "c":25, "r":21},
			"55591" : {"s":"Nex 0003", "c":25, "r":20},
			"56031" : {"s":"Ophiuchi", "c":22, "r":20},
			"56606" : {"s":"Paan", "c":25, "r":23},
			"56830" : {"s":"Quince", "c":14, "r":16},
			"57070" : {"s":"Sodaack", "c":15, "r":16},
			"57495" : {"s":"Tiliala", "c":25, "r":17},
			"57684" : {"s":"Urioed", "c":21, "r":9},
			"58159" : {"s":"Veareth", "c":19, "r":25},
			"58439" : {"s":"Waarze", "c":20, "r":14},
			"58621" : {"s":"ZP 2-989", "c":13, "r":14},
			"59246" : {"s":"Pass EMP-04", "c":25, "r":25},
			"59506" : {"s":"Pass EMP-05", "c":13, "r":20},
			"78720" : {"s":"Aandti", "c":22, "r":13},
			"79260" : {"s":"Anphiex", "c":18, "r":30},
			"79575" : {"s":"Atlas", "c":21, "r":15},
			"79767" : {"s":"Baar", "c":16, "r":12},
			"80267" : {"s":"Becanol", "c":20, "r":25},
			"81011" : {"s":"BL 6-511", "c":24, "r":31},
			"81636" : {"s":"Edenve", "c":25, "r":25},
			"81804" : {"s":"Faarfa", "c":14, "r":12},
			"82182" : {"s":"Gilo", "c":18, "r":21},
			"82507" : {"s":"Hooth", "c":25, "r":13},
			"82747" : {"s":"Ioquex", "c":16, "r":15},
			"83051" : {"s":"Lasolia", "c":19, "r":16},
			"83626" : {"s":"Nex 0001", "c":23, "r":25},
			"83766" : {"s":"Polaris", "c":10, "r":14},
			"84066" : {"s":"Qumia", "c":20, "r":15},
			"84691" : {"s":"Solaqu", "c":25, "r":25},
			"84951" : {"s":"UZ 8-466", "c":20, "r":13},
			"85576" : {"s":"Waolex", "c":25, "r":25},
			"85856" : {"s":"Zelada", "c":14, "r":20},
			"86261" : {"s":"Pass FED-07", "c":27, "r":15},
			"95533" : {"s":"Adara", "c":15, "r":21},
			"95833" : {"s":"Alfirk", "c":20, "r":15},
			"96233" : {"s":"Diphda", "c":20, "r":20},
			"96443" : {"s":"EH 5-382", "c":14, "r":15},
			"96763" : {"s":"HW 3-863", "c":16, "r":20},
			"97035" : {"s":"Kitalpha", "c":17, "r":16},
			"97375" : {"s":"Mebsuta", "c":17, "r":20},
			"98000" : {"s":"Nex 0004", "c":25, "r":25},
			"98475" : {"s":"Nusakan", "c":25, "r":19},
			"98895" : {"s":"Phao", "c":21, "r":20},
			"99199" : {"s":"Rotanev", "c":16, "r":19},
			"99505" : {"s":"Seginus", "c":17, "r":18},
			"100130" : {"s":"Thabit", "c":25, "r":25},
			"100605" : {"s":"Wasat", "c":25, "r":19},
			"100843" : {"s":"Yildun", "c":14, "r":17},
			"101099" : {"s":"Zaniah", "c":16, "r":16},
			"101524" : {"s":"Zuben Elakrab", "c":25, "r":17},
			"101854" : {"s":"Ackwada", "c":22, "r":15},
			"102109" : {"s":"Aveed", "c":17, "r":15},
			"102589" : {"s":"Beta Hydri", "c":24, "r":20},
			"102797" : {"s":"CP 2-197", "c":16, "r":13},
			"103121" : {"s":"Dainfa", "c":18, "r":18},
			"103427" : {"s":"Daured", "c":18, "r":17},
			"103827" : {"s":"DI 9-486", "c":25, "r":16},
			"104452" : {"s":"Edethex", "c":25, "r":25},
			"104870" : {"s":"Exiool", "c":22, "r":19},
			"105270" : {"s":"Gretiay", "c":20, "r":20},
			"105558" : {"s":"Ioliaa", "c":18, "r":16},
			"105966" : {"s":"Liaququ", "c":17, "r":24},
			"106306" : {"s":"Menkent", "c":20, "r":17},
			"106612" : {"s":"Naos", "c":17, "r":18},
			"107068" : {"s":"Quexce", "c":19, "r":24},
			"107476" : {"s":"Sophilia", "c":24, "r":17},
			"107936" : {"s":"Spica", "c":20, "r":23},
			"108396" : {"s":"Urfaa", "c":23, "r":20},
			"109606" : {"s":"Vega", "c":30, "r":25},
			"109878" : {"s":"Wainze", "c":17, "r":16},
			"110094" : {"s":"YV 3-386", "c":12, "r":18},
			"110553" : {"s":"Zaurak", "c":17, "r":27},
			"110761" : {"s":"DH 3-625", "c":16, "r":13},
			"111086" : {"s":"Pass EMP-06", "c":25, "r":13},
			"111486" : {"s":"Pass UNI-01", "c":25, "r":16},
			"111586" : {"s":"Pass UNI-02", "c":10, "r":10},
			"111946" : {"s":"Pass UNI-03", "c":18, "r":20},
			"119021" : {"s":"Achird", "c":22, "r":22},
			"119421" : {"s":"BE 3-702", "c":20, "r":20},
			"119871" : {"s":"Bellatrix", "c":25, "r":18},
			"120375" : {"s":"Cebalrai", "c":21, "r":24},
			"120664" : {"s":"Dsiban", "c":17, "r":17},
			"120964" : {"s":"Furud", "c":15, "r":20},
			"121354" : {"s":"Gienah Cygni", "c":15, "r":26},
			"121728" : {"s":"Homam", "c":17, "r":22},
			"122016" : {"s":"Izar", "c":16, "r":18},
			"122416" : {"s":"Keid", "c":20, "r":20},
			"122948" : {"s":"Lazebe", "c":28, "r":19},
			"123204" : {"s":"Matar", "c":16, "r":16},
			"123708" : {"s":"Nashira", "c":24, "r":21},
			"124044" : {"s":"Nekkar", "c":14, "r":24},
			"124296" : {"s":"Olaeth", "c":18, "r":14},
			"124568" : {"s":"Phaet", "c":17, "r":16},
			"125318" : {"s":"Sirius", "c":30, "r":25},
			"125718" : {"s":"Subra", "c":20, "r":20},
			"126178" : {"s":"Turais", "c":20, "r":23},
			"126684" : {"s":"UG 5-093", "c":22, "r":23},
			"127084" : {"s":"Wezen", "c":20, "r":20},
			"127260" : {"s":"WW 2-934", "c":16, "r":11},
			"127885" : {"s":"Pass UNI-04", "c":25, "r":25},
			"128535" : {"s":"Pass UNI-05", "c":25, "r":26},
			"138412" : {"s":"Nari", "c":34, "r":37},
			"139287" : {"s":"Enif", "c":35, "r":25},
			"140331" : {"s":"Baham", "c":29, "r":36},
			"140817" : {"s":"Cebece", "c":27, "r":18},
			"142497" : {"s":"Cor Caroli", "c":40, "r":42},
			"142997" : {"s":"Dubhe", "c":20, "r":25},
			"144397" : {"s":"Aya", "c":40, "r":35},
			"145141" : {"s":"Etamin", "c":31, "r":24},
			"145891" : {"s":"Fornacis", "c":25, "r":30},
			"146581" : {"s":"Gomeisa", "c":30, "r":23},
			"148004" : {"s":"Heze", "c":35, "r":40},
			"149296" : {"s":"Labela", "c":34, "r":38},
			"150214" : {"s":"Menkar", "c":27, "r":34},
			"151214" : {"s":"Mintaka", "c":40, "r":25},
			"160514" : {"s":"Pardus", "c":100, "r":93},
			"161634" : {"s":"Nhandu", "c":28, "r":40},
			"162781" : {"s":"Procyon", "c":37, "r":31},
			"163481" : {"s":"Quaack", "c":28, "r":25},
			"165121" : {"s":"Ras elased", "c":41, "r":40},
			"166787" : {"s":"Rigel", "c":49, "r":34},
			"167637" : {"s":"Sargas", "c":34, "r":25},
			"168150" : {"s":"Nunki", "c":19, "r":27},
			"168650" : {"s":"Meram", "c":20, "r":25},
			"300279" : {"s":"Ackarack", "c":14, "r":20},
			"300519" : {"s":"Anayed", "c":15, "r":16},
			"300919" : {"s":"Ayinti", "c":20, "r":20},
			"301159" : {"s":"Beeday", "c":16, "r":15},
			"301559" : {"s":"Belati", "c":25, "r":16},
			"301759" : {"s":"CC 3-771", "c":20, "r":10},
			"301983" : {"s":"DP 2-354", "c":16, "r":14},
			"302751" : {"s":"Edvea", "c":32, "r":24},
			"303251" : {"s":"Fawaol", "c":20, "r":25},
			"303571" : {"s":"Greliai", "c":16, "r":20},
			"303875" : {"s":"Hource", "c":19, "r":16},
			"304091" : {"s":"Iowagre", "c":18, "r":12},
			"304491" : {"s":"Miackio", "c":25, "r":16},
			"304815" : {"s":"Oldain", "c":18, "r":18},
			"305053" : {"s":"Quaphi", "c":17, "r":14},
			"305209" : {"s":"RX 3-129", "c":13, "r":12},
			"305559" : {"s":"Tiurio", "c":25, "r":14},
			"305874" : {"s":"Watibe", "c":21, "r":15},
			"306154" : {"s":"YS 3-386", "c":14, "r":20},
			"306426" : {"s":"Zearla", "c":17, "r":16},
			"306686" : {"s":"ZS 3-798", "c":13, "r":20},
			"307026" : {"s":"Aedce", "c":17, "r":20},
			"307250" : {"s":"Ayqugre", "c":16, "r":14},
			"307505" : {"s":"Ceanze", "c":15, "r":17},
			"307778" : {"s":"Enioar", "c":21, "r":13},
			"308128" : {"s":"Faedho", "c":14, "r":25},
			"308394" : {"s":"Hobeex", "c":19, "r":14},
			"308576" : {"s":"Inioen", "c":13, "r":14},
			"308736" : {"s":"JG 2-013", "c":20, "r":8},
			"308976" : {"s":"Leesti", "c":15, "r":16},
			"309376" : {"s":"Liaface", "c":20, "r":20},
			"309614" : {"s":"Ollaffa", "c":17, "r":14},
			"309974" : {"s":"Qumiin", "c":18, "r":20},
			"310118" : {"s":"RA 3-124", "c":12, "r":12},
			"310404" : {"s":"SF 5-674", "c":13, "r":22},
			"310824" : {"s":"Soolti", "c":21, "r":20},
			"311472" : {"s":"Tiafa", "c":24, "r":27},
			"311668" : {"s":"UF 3-555", "c":14, "r":14},
			"311974" : {"s":"Ureneth", "c":18, "r":17},
			"312224" : {"s":"VM 3-326", "c":25, "r":10},
			"312599" : {"s":"Wamien", "c":25, "r":15},
			"312855" : {"s":"Xewao", "c":16, "r":16},
			"313430" : {"s":"Pass EMP-07", "c":25, "r":23},
			"313955" : {"s":"Pass EMP-08", "c":25, "r":21},
			"314580" : {"s":"Pass EMP-09", "c":25, "r":25},
			"315205" : {"s":"Pass EMP-10", "c":25, "r":25},
			"315535" : {"s":"Pass EMP-11", "c":15, "r":22},
			"315857" : {"s":"Pass FED-08", "c":14, "r":23},
			"316248" : {"s":"Pass FED-09", "c":23, "r":17},
			"316628" : {"s":"Pass FED-10", "c":19, "r":20},
			"317002" : {"s":"Pass FED-11", "c":22, "r":17},
			"317464" : {"s":"Pass FED-12", "c":21, "r":22},
			"317787" : {"s":"Pass UNI-06", "c":17, "r":19},
			"318339" : {"s":"Pass UNI-07", "c":23, "r":24},
			"318959" : {"s":"Pass UNI-08", "c":20, "r":31},
			"319409" : {"s":"Andsoled", "c":18, "r":25},
			"319884" : {"s":"Beurso", "c":19, "r":25},
			"320124" : {"s":"Ceina", "c":16, "r":15},
			"320379" : {"s":"Daaze", "c":17, "r":15},
			"320549" : {"s":"Edqueth", "c":17, "r":10},
			"320989" : {"s":"Enwaand", "c":20, "r":22},
			"321229" : {"s":"FR 3-328", "c":12, "r":20},
			"321689" : {"s":"Ladaen", "c":20, "r":23},
			"322149" : {"s":"Liaackti", "c":20, "r":23},
			"322501" : {"s":"Oauress", "c":22, "r":16},
			"322981" : {"s":"Phiandgre", "c":24, "r":20},
			"323219" : {"s":"Quexho", "c":17, "r":14},
			"323475" : {"s":"Stein", "c":16, "r":16},
			"323975" : {"s":"Tivea", "c":25, "r":20},
			"324185" : {"s":"Veedfa", "c":14, "r":15},
			"324425" : {"s":"Canolin", "c":16, "r":15},
			"325050" : {"s":"Nex 0005", "c":25, "r":25},
			"325245" : {"s":"PP 5-713", "c":15, "r":13},
			"325644" : {"s":"Sowace", "c":19, "r":21},
			"325969" : {"s":"Abeho", "c":25, "r":13},
			"326258" : {"s":"Arexack", "c":17, "r":17},
			"326458" : {"s":"BU 5-773", "c":25, "r":8},
			"326836" : {"s":"Dadaex", "c":18, "r":21},
			"327311" : {"s":"Famida", "c":25, "r":19},
			"327836" : {"s":"Gresoin", "c":25, "r":21},
			"328060" : {"s":"GT 3-328", "c":14, "r":16},
			"328444" : {"s":"Iohofa", "c":24, "r":16},
			"328764" : {"s":"Lagreen", "c":16, "r":20},
			"328948" : {"s":"Lavebe", "c":23, "r":8},
			"329696" : {"s":"Let", "c":22, "r":34},
			"330171" : {"s":"Miola", "c":25, "r":19},
			"330671" : {"s":"Olaso", "c":25, "r":20},
			"331011" : {"s":"PA 2-013", "c":20, "r":17},
			"331191" : {"s":"Sobein", "c":15, "r":12},
			"331438" : {"s":"Tigrecan", "c":19, "r":13},
			"331778" : {"s":"Uressce", "c":20, "r":17},
			"332108" : {"s":"Uv Seti", "c":22, "r":15},
			"332508" : {"s":"Veliace", "c":25, "r":16},
			"332844" : {"s":"WI 4-329", "c":16, "r":21},
			"333222" : {"s":"Zeaay", "c":27, "r":14},
			"375251" : {"s":"AB 5-848", "c":18, "r":14},
			"375726" : {"s":"Algol", "c":19, "r":25},
			"376076" : {"s":"Bewaack", "c":14, "r":25},
			"376472" : {"s":"Cegreeth", "c":18, "r":22},
			"376744" : {"s":"Edmial", "c":17, "r":16},
			"377194" : {"s":"Elnath", "c":18, "r":25},
			"377819" : {"s":"Fadaphi", "c":25, "r":25},
			"378139" : {"s":"Greenso", "c":20, "r":16},
			"378539" : {"s":"JO 4-132", "c":20, "r":20},
			"378964" : {"s":"Miayda", "c":25, "r":17},
			"379589" : {"s":"Nex 0006", "c":25, "r":25},
			"379814" : {"s":"Oucanfa", "c":15, "r":15},
			"380134" : {"s":"Propus", "c":16, "r":20},
			"380634" : {"s":"Silaad", "c":25, "r":20},
			"381024" : {"s":"Vecelia", "c":15, "r":26},
			"381296" : {"s":"Xeho", "c":16, "r":17},
			"381582" : {"s":"ZU 3-239", "c":13, "r":22},
			"381918" : {"s":"Pass FED-13", "c":16, "r":21},
			"382218" : {"s":"Pass UNI-09", "c":20, "r":15}
		};
		for (key in locref) {
			if (loc <= key) {
				var start_ref = key - locref[key].r * locref[key].c + 1;
				var delta = loc - start_ref;
				return {
					"s": locref[key].s,
					"x": Math.floor(delta / locref[key].r),
					"y": delta % locref[key].r
				};
			}
		}
	};

	var loc = GetSectorCoords(userloc);
	var span = document.createElement('span');
	span.innerHTML = " " + loc.s + " [" + loc.x + "," + loc.y + "]<br/>";
	span.setAttribute('style','color:gold;font-size:11px;');
	var node = document.querySelector('br');
	node.parentNode.insertBefore(span, node.nextElementSibling);
}

